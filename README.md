

This Github Browser app demonstrates how:

*  Angular 1.5 component based architecture can use ng-transclude directive
*  $httpbackend can provide fake data for unit testing
*  Angular $filter can provide re-usable and fast string formatting for presentation

*  Sass can help simplify and organize styles for easier maintenance
*  Browserify can bundle CommonJS module packages (including npm packages - no more bower)
*  CacheBust can prevent browsers from using previously cached assets
*  Gulp data streams can speed up build scripts
*  Gulp tools allows a more developer friendly organization of files
*  Gulp watch can monitor development activity and automatically re-build specific files

*  Angular Material `layout` and `flex` options can easily configure HTML containers
*  Angular Material components `<md-toolbar>`, `<md-sidenav>`, `<md-icon>` can be quickly used
*  Custom controller can easily, programmatically open & close the SideNav component.
*  Responsive breakpoints and `$mdMedia` are used
*  Theming can be altered/configured using `$mdThemingProvider`
*  ARIA features are supported by Angular Material and warnings can be used to improve accessibility.


#### Install Dependencies

npm install


## Directory Layout

```
app/                    	--> all of the source files for the application
	assets/					--> all svg and third party css
	css/app.scss        	--> default stylesheet
	src/           			--> all app specific modules
		repos/            	--> package for repo module
	index.html           	--> app layout file (the main html template file of the app)
dist/						--> all output files ready for deployment to server
	maps/					--> source maps for minified files
test/
	karma.conf.js         	--> Karma-Jasmine config file (for unit tests)
	protractor-conf.js    	--> Protractor config file (for e2e tests)
	unit/					--> Karma-Jasmine unit tests
	e2e/ 			        --> end-to-end tests
```




### Running the App during Development

gulp dev

### Build for production

gulp prod

## Serving the Application Files

gulp server

### Run UnitTests

gulp karma
-run tests in Chrome

gulp karma --debug
-run tests in debug mode

gulp karma --phantom
-run tests in phantomjs



## Contact

For more information on AngularJS please check out http://angularjs.org/
For more information on Angular Material, check out https://material.angularjs.org/

[git]: http://git-scm.com/
[npm]: https://www.npmjs.org/
[node]: http://nodejs.org
[protractor]: https://github.com/angular/protractor
[jasmine]: http://jasmine.github.io
[karma]: http://karma-runner.github.io
[travis]: https://travis-ci.org/
