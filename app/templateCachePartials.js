(function(module) {
try {
  module = angular.module('views');
} catch (e) {
  module = angular.module('views', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/views/listView/listView.html',
    '<div><md-toolbar layout="row" class="md-hue-3"><div class="md-toolbar-tools"><span>{{$ctrl.title}}</span></div></md-toolbar><div ng-if="!$ctrl.list" layout="row" layout-sm="column" layout-align="center center"><md-progress-circular md-mode="indeterminate"></md-progress-circular></div><md-list ng-if="$ctrl.list"><div ng-transclude=""></div></md-list></div>');
}]);
})();
