'use strict';

var angular = require('angular');

require('../../templateCachePartials');

angular.module('components', ['views']);

require('./listView');