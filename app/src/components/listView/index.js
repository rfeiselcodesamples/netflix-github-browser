'use strict';
  
angular.module('components')
.component('listView', {
  	bindings: {
  	  list: '<' ,
  	  title: '<?'
  	},
  	transclude: true,
  	templateUrl: '/views/listView/listView.html'
  });