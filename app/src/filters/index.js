'use strict';

var angular = require('angular');
var Capitalize = require('./capitalize');

var listView = angular.module('filters', []).filter('capitalize', Capitalize);