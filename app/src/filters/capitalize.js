'use strict'

 /**
   * Simple angular filter function to 'capitalize' the first char in the string
   */
  module.exports = function Capitalize( ) {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
  }};