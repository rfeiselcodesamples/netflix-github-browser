'use strict';

/**
 * Repos DataService
 *
 * @returns {{loadAllRepos: Function}, {loadAllCommits: Function}}
 * @constructor
 */
module.exports = function RepoService($q, $http){
  
  var url = 'https://api.github.com';

  // Promise-based API
  this.loadAllRepos = function(organization){
    return $http.get(url + '/orgs/' + organization + '/repos')
      .then(function(result){
        return result.data;
      });
  }

  this.loadAllCommits = function(repo){
    return $http.get(url + '/repos/' + repo.owner.login + '/' + repo.name + '/commits')
      .then(function(result){
        return result.data;
      });
  }

};