'use strict';

/**
 * Main Controller for the Github Repo Browser App
 * @param repoService
 * @param $mdSidenav
 * @param $log
 * @param $filter
 * @constructor
 */
module.exports = function RepoController( repoService, $mdSidenav, $log, $filter) {
  var self = this;
  var orderBy = $filter('orderBy');
  
  self.selected = {}; //object told user selection

  self.selected.repo     = null;
  self.selected.orgName = 'spinnaker'; //use 'spinnaker' as default for this demo
  
  self.repos  = [ ];
  self.commits  = [ ];
  
  self.selectOrg  = selectOrg;
  self.selectRepo  = selectRepo;
  
  self.toggleList   = toggleList;

  // Load all repos for default org ('spinnaker')
  loadRepos();
    

  // *********************************
  // Internal methods
  // *********************************

  /**
   * load all repos for selected org
   */
  function loadRepos() {
    if(self.selected.orgName){
      repoService
      .loadAllRepos(self.selected.orgName)
      .then( function( repos ) {
        self.repos    = [].concat(repos);
      })
      .then( function(){
        self.repos = orderBy(self.repos, 'forks_count', true);
      })
      .then( function(){
        self.selected.repo = self.repos[0];
        loadCommits();
      });
    }
  }
  
  /**
   * load all commits for selected repo
   */
  function loadCommits() {
    repoService
      .loadAllCommits(self.selected.repo)
      .then( function( commits ) {
        self.commits    = [].concat(commits);
        self.commits = orderBy(self.commits, 'commit.author.date', true);
     
      });
  }

  /**
   * Hide or Show the 'left' sideNav area
   */
  function toggleList() {
    $mdSidenav('left').toggle();
  }

  /**
   * Select the current repo
   * @param repo
   */
  function selectRepo ( repo ) {
    //reset presentation data
    self.commits = null;
    
    //espress newly selected presentation data
    self.selected.repo = repo;
    loadCommits();
    
    //update UI layout - close sidenav
    self.toggleList();
  }
  
   /**
   * Select the current organization
   * @param organization
   */
  function selectOrg ( organization ) {
    //reset presentation data
    self.repos = null;
    //express new presentation data
    self.selected.orgName = organization;
    loadRepos();
  }
};
