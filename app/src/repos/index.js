'use strict';
var angular = require('angular');

var ngMaterial = require('angular-material');
var repos = angular.module('repos', [ 'ngMaterial' ]);

repos.service('repoService', ['$q', '$http', require('./RepoService')]); 
repos.controller('RepoController', ['repoService', '$mdSidenav', '$log', '$filter', require('./RepoController')]);