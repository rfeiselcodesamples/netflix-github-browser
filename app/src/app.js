'use strict';

var angular = require('angular');
var ngMaterial = require('angular-material');

var components = require('./components');
var filters = require('./filters');

var repos = require('./repos');


angular
  .module('githubBrowser', ['ngMaterial', 'repos', 'components', 'filters'])
  .config(function($mdThemingProvider, $mdIconProvider){

      $mdIconProvider
          .defaultIconSet("./assets/svg/avatars.svg", 128)
          .icon("menu", "./assets/svg/menu.svg", 24);

      $mdThemingProvider.theme('default')
          .primaryPalette('blue')
          .accentPalette('grey');

  });