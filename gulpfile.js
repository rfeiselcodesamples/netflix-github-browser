'use strict';

var browserify = require('browserify')
  , del = require('del')
  , source = require('vinyl-source-stream')
  , vinylPaths = require('vinyl-paths')
  , buffer = require('vinyl-buffer')
  , glob = require('glob')
  , gulp = require('gulp');
  
var CacheBuster = require('gulp-cachebust');
var cachebust = new CacheBuster();

var liveReload = true;

// Load all gulp plugins listed in package.json
var gulpPlugins = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'gulp.*'],
  replaceString: /\bgulp[\-.]/
});

// Define file path variables
var paths = {
  root: 'app/',                 // App root path
  src:  'app/src/',             // Source path
  html: 'app/src/components/**/*.html',  // html paths
  scss: 'app/scss/*.scss',           // scss path
  svg: 'app/assets/svg/*.svg',  // svg path
  assets: 'app/assets/**/*.*',  // assets path
  components: 'app/src/components', // components path
  thirdParty: 'node_modules/angular-material/angular-material.min.css',
  dist: 'dist/',            // Distribution path
  test: 'test/',                // Test path
};


/*
 * Useful tasks:
 * - gulp:
 *   - linting
 *   - unit tests
 *   - browserification
 *   - no minification, does not start server.
 *   - start server for e2e tests
 *   - run Protractor End-to-end tests
 *   - stop server immediately when e2e tests have finished
 * - gulp dev:
 *   - starts server with live reload enabled and automatic re-building using watch
 *   - lints, unit tests, browserifies and live-reloads changes in browser
 *   - no minification
 * - gulp prod:
 *   - linting
 *   - unit tests
 *   - minification and browserification of minified sources
 *
 * At development time, you should usually just have 'gulp dev' running in the
 * background all the time. Use 'gulp prod' before releases.
 */

gulp.task('default', ['clean'], function () {
  gulp.start('build-html', 'karma', 'e2e');
});

gulp.task('dev', ['clean'], function () {
  liveReload = true;
  gulp.start('build-html', 'watch-js', 'watch-assets', 'watch-scss', 'watch-html', 'watch-index', 'server');
});

gulp.task('prod', ['clean'], function () {
  gulp.start('build-prod-html', 'karma' /*, 'e2e'*/);
});


/////////////////////////////////////////////////////////////////////////////////////
//
// runs clean
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('clean', function () {
  return gulp
    .src([paths.root + 'ngAnnotate', paths.dist], {read: false})
    .pipe(vinylPaths(del));
});

gulp.task('clean-css', function () {
  return gulp
    .src([paths.dist + '*.css'], {read: false})
    .pipe(vinylPaths(del));
});

gulp.task('clean-js', function () {
  return gulp
    .src([paths.dist + '*.js'], {read: false})
    .pipe(vinylPaths(del));
});

gulp.task('clean-assets', function () {
  return gulp
    .src([paths.dist + 'assets'], {read: false})
    .pipe(vinylPaths(del));
});


/////////////////////////////////////////////////////////////////////////////////////
//
// runs sass, creates css source maps
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('build-css', function() {
  return gulp.src( paths.scss )
      .pipe(gulpPlugins.sourcemaps.init())
      .pipe(gulpPlugins.sass())
      .pipe(gulpPlugins.concat('app.css'))
      .pipe(cachebust.resources())
      .pipe(gulpPlugins.sourcemaps.write('./maps'))
      .pipe(gulp.dest( paths.dist ))
      //.pipe(gulpPlugins.notify({ message: 'Styles task complete' }));
});

gulp.task('copy-assets', ['clean-assets'], function () {
  //copy third party
  gulp.src( paths.thirdParty )
        .pipe(gulp.dest( paths.dist + 'assets/'));
  //copy svg
  gulp.src([ paths.svg ])
        .pipe(gulp.dest( paths.dist + 'assets/svg'));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// fills in the Angular template cache, to prevent loading the html templates via
// separate http requests
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('build-template-cache', function() {
  return gulp.src( paths.html )
    .pipe(gulpPlugins.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe(gulpPlugins.ngHtml2js({
      //declareModule: 'false',
      //export: 'commonjs',
      moduleName: "views",
      prefix: "/views/"
    }))
    .pipe(gulpPlugins.concat("templateCachePartials.js"))
    .pipe(gulp.dest( paths.root ));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// runs jshint
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('jshint', function() {
  gulp.src('/app/src/**/*.js')
      .pipe(gulpPlugins.jshint())
      .pipe(gulpPlugins.jshint.reporter('default'));
});

gulp.task('lint', function () {
  return gulp
  .src(['gulpfile.js',
      paths.src + '**/*.js',
      paths.test + '**/*.js',
      '!' + paths.src + 'third-party/**',
      '!' + paths.test + 'browserified/**',
  ])
  .pipe(gulpPlugins.eslint())
  .pipe(gulpPlugins.eslint.format());
});

gulp.task('browserify', ['build-template-cache', 'lint', 'jshint'], function () {
  return browserify(paths.src + 'app.js', {debug: true})
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(cachebust.resources())
    .pipe(gulpPlugins.sourcemaps.init({loadMaps: true}))
    //.on('error', gulpPlugins.util.log)
    .pipe(gulpPlugins.sourcemaps.write('./maps'))
    .pipe(gulp.dest( paths.dist ))
    .pipe(gulpPlugins.connect.reload())
    //.pipe(gulpPlugins.notify({ message: 'Browserify task complete' }));
});

gulp.task('ngAnnotate', ['build-template-cache', 'lint', 'jshint'], function () {
  return gulp.src([
      paths.src + '**/*.js',
      '!' + paths.src + 'third-party/**',
  ])
  .pipe(gulpPlugins.ngAnnotate())
  .pipe(gulp.dest(paths.root + 'ngAnnotate'));
});

gulp.task('browserify-min', ['ngAnnotate'], function () {
  return browserify(paths.root + 'ngAnnotate/app.js')
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(cachebust.resources())
    .pipe(gulpPlugins.sourcemaps.init({loadMaps: true}))
    .pipe(gulpPlugins.streamify(gulpPlugins.uglify({mangle: true})))
    .pipe(gulpPlugins.sourcemaps.write('./maps'))
    .pipe(gulp.dest(paths.dist ))
    .pipe(gulpPlugins.notify({ message: 'Browserify-min task complete' }));
});

gulp.task('browserify-tests', ['build-template-cache'], function () {
  var bundler = browserify({debug: true});
  glob
  .sync(paths.test + 'unit/**/*.js')
  .forEach(function (file) {
    bundler.add(file);
  });
  return bundler
  .bundle()
  .pipe(source('browserified_tests.js'))
  .pipe(gulp.dest(paths.test + 'browserified'));
});

gulp.task('karma', ['browserify-tests'], function (done) {
  var Server = require('karma').Server;
  var conf = gulpPlugins.util.env.phantom ? 'karma.phantom.conf.js' : 'karma.conf.js';
  var singleRun = gulpPlugins.util.env.debug ? false : true;

  var options = {
      configFile: __dirname + '/test/' + conf,
      singleRun: singleRun
  }

  new Server(options, done).start();

});

gulp.task('server', function () {
  var options = {
    root: paths.dist,
    livereload: liveReload,
  }
  return gulpPlugins.connect.server(options);
});

gulp.task('e2e', ['server'], function () {
  return gulp.src([paths.test + 'e2e/**/*.js'])
  .pipe(gulpPlugins.protractor.protractor({
    configFile: 'protractor.conf.js',
    args: ['--baseUrl', 'http://127.0.0.1:8080'],
  }))
  .on('error', function (e) {
    throw e;
  })
  .on('end', function () {
    gulpPlugins.connect.serverClose();
  });
});

gulp.task('watch-js', function () {
  gulp.watch([
    paths.src + '**/*.js'
    , paths.components + '**/*.js'
    , '!' + paths.src + 'third-party/**'
  ], ['update-js']);
});

gulp.task('watch-assets', function () {
  gulp.watch([
    paths.assets
  ], ['copy-assets']);
});

gulp.task('watch-scss', function () {
  gulp.watch([
    paths.scss
  ], ['update-css']);

});

gulp.task('watch-index', function () {
  gulp.watch([
    paths.root + 'index.html',
  ], ['update-html']);
});

gulp.task('watch-html', function () {
  gulp.watch([
    paths.html,
  ], ['update-js']);
});

gulp.task('update-js', ['browserify', 'clean-js'], function () {
  return gulp.start('update-html');
});


gulp.task('update-css', ['build-css', 'clean-css'], function () {
  return gulp.start('update-html');
});

gulp.task('update-html', function () {
  return gulp.src(paths.root + 'index.html')
        .pipe(cachebust.references())
        .pipe(gulp.dest( paths.dist ))
        .pipe(gulpPlugins.connect.reload());
});

gulp.task('build-html', ['build-css', 'browserify', 'copy-assets'], function () {
  return gulp.src(paths.root + 'index.html')
        .pipe(cachebust.references())
        .pipe(gulp.dest( paths.dist ))
        .pipe(gulpPlugins.connect.reload());
});

gulp.task('build-prod-html', ['clean', 'build-css', 'browserify-min', 'copy-assets'], function () {
  return gulp.src(paths.root + 'index.html')
        .pipe(cachebust.references())
        .pipe(gulp.dest( paths.dist ));
});


function handleErrors() {
  var args = Array.prototype.slice.call(arguments);
  gulpPlugins.notify.onError({
    title: "Compile Error",
    message: "<%= error.message %>"
  }).apply(this, args);
  this.emit('end'); // Keep gulp from hanging on this task
}
